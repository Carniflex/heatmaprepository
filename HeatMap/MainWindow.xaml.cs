﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using Telerik.Windows.Documents.FormatProviders.Pdf;
using Telerik.Windows.Documents.Layout;
using Telerik.Windows.Documents.Model;


namespace HeatMap
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly HeatMapVm _vm = new HeatMapVm(ShowText);

        private static void ShowText(string obj)
        {
            MessageBox.Show(obj);
        }

        public MainWindow()
        {
            _vm.LoadData();
            DataContext = _vm;
            InitializeComponent();
      }


        private void RadButton_Click(object sender, RoutedEventArgs e)
        {
            PrintDialog printDialog = new PrintDialog();
            if (printDialog.ShowDialog() == true)
            {
                printDialog.PrintVisual(CanvasForTreeMap, "Print");
            }
        }

        private void RadButton_Click_1(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.FileName = "Document"; // Default file name
            dlg.DefaultExt = ".jpeg"; // Default file extension
            dlg.Filter = "Jpeg image(.jpeg)|*.jpeg"; // Filter files by extension 

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results 
            if (result == true)
            {
                // Save document 
                string filename = dlg.FileName;
                CreateSaveBitmap(CanvasForTreeMap, filename);
            }
        }

        private void CreateSaveBitmap(Canvas canvas, string filename)
        {
            RenderTargetBitmap renderBitmap = new RenderTargetBitmap(
             (int)canvas.Width, (int)canvas.Height,
             96d, 96d, PixelFormats.Pbgra32);
            // needed otherwise the image output is black
            canvas.Measure(new Size((int)canvas.Width, (int)canvas.Height));
            canvas.Arrange(new Rect(new Size((int)canvas.Width, (int)canvas.Height)));
            renderBitmap.Render(canvas);
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            //PngBitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(renderBitmap));
            using (FileStream file = File.Create(filename))
            {
                encoder.Save(file);
            }
        }

        private RadDocument CreateDocument()
        {
            RadDocument document = new RadDocument();
            CreateChartDocumentPart(document);
            return document;
        }

        private void CreateChartDocumentPart(RadDocument document)
        {
            var section = new Section();
            var sec = new Section();
            var paragraph = new Paragraph();
            var parag = new Paragraph();
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            using (MemoryStream ms = new MemoryStream())
            {

                ExportToImage(CanvasForTreeMap, ms);
                ms.Seek(0, SeekOrigin.Begin);
                bi.SetSource(ms);
                bi.CreateOptions = BitmapCreateOptions.None;
                bi.EndInit(); var wb = new WriteableBitmap(bi);
                ImageInline image = new ImageInline(wb) { Width = 700, Height = 500 };
                paragraph.Inlines.Add(image);
                section.Blocks.Add(paragraph);
                document.Sections.Add(section);

            }



        }
        private void ExportToImage(Canvas canvas, MemoryStream stream)
        {
            RenderTargetBitmap renderBitmap = new RenderTargetBitmap(
                 (int)canvas.Width, (int)canvas.Height,
                 96d, 96d, PixelFormats.Pbgra32);

            canvas.Measure(new Size((int)canvas.Width, (int)canvas.Height));
            canvas.Arrange(new Rect(new Size((int)canvas.Width, (int)canvas.Height)));
            renderBitmap.Render(canvas);
            var encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(renderBitmap));
            encoder.Save(stream);


        }

        private void RadButton_Click_2(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.DefaultExt = "*.pdf";
            dialog.Filter = "Adobe PDF Document (*.pdf)|*.pdf";
            Nullable<bool> result = dialog.ShowDialog();
            if (result == true)
            {
                string filename = dialog.FileName;
                RadDocument document = this.CreateDocument();

                document.LayoutMode = DocumentLayoutMode.Flow;
                document.Arrange(new RectangleF(PointF.Empty, document.DesiredSize));
                PdfFormatProvider provider = new PdfFormatProvider();
                using (Stream output = dialog.OpenFile())
                {
                    provider.Export(document, output);
                }
            }
        }

        private void MaximizegridButton_Click(object sender, RoutedEventArgs e)
        {
            //ScaleTransform scale = new ScaleTransform();
            //e.Handled = true;
            //scale.ScaleX *= 1.1;
            //scale.ScaleY = scale.ScaleX;
            //zoomViewer.Margin = position;
        }





    }
}
