﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using DataGen;


namespace HeatMap
{
    public class HeatMapVm : INotifyPropertyChanged
    {
        private readonly DataProvider _dataProvider = new DataProvider();

        private ObservableCollection<ICompany> _heatMapItems;
        private ObservableCollection<ComboItem> _sizes;
        private ObservableCollection<ComboItem> _colories; 

        private string _styleName;
        private string _groupName = "None";
        private string _displayLabel = "Company Name";
        private string _filterKey = "";
        private string _addsymbol = "All Market";
        private string _displayValue = "None";

        private ComboItem _selectedSize=new ComboItem();
        private ComboItem _selectedColor=new ComboItem();

        private Action<string> ShowMessage;

        /// <summary>
        /// Load data from provider
        /// </summary>
        public void LoadData()
        {
            var items = _dataProvider.GetData(_filterKey, _groupName,_selectedSize, _selectedColor);

            if (items.Count() != 0)//need to fix issue - updating after filter  
            {
                _heatMapItems = new ObservableCollection<ICompany>(items);

                //Take all distinct sizes from heatmapitems
                 var sizes = new List<double>(
                    _heatMapItems
                    .OrderByDescending(item=>item.SizeValue)
                    .Select(item => item.SizeValue)
                    .Distinct());
                _sizes=new ObservableCollection<ComboItem>();
                _sizes.Insert(0,new ComboItem());
                foreach (var size in sizes)
                {
                    _sizes.Add(new ComboItem(size));
                }
                OnPropertyChanged("Sizes");

                //Take all distinct color values from heatmapitems
                var colories = new ObservableCollection<double>(
                    _heatMapItems
                    .OrderByDescending(item => item.ColorValue)
                    .Select(item => item.ColorValue)
                    .Distinct());
                _colories=new ObservableCollection<ComboItem>();
                _colories.Insert(0,new ComboItem());
                foreach (var color in colories)
                {
                    _colories.Add(new ComboItem(color));
                }
                OnPropertyChanged("Colories");
            }                      
        }

        /// <summary>
        /// Update data from provider and render in view
        /// </summary>
        private void UpdateData()
        {
            LoadData();
            DataProvider.GetDataByCompanyType(_heatMapItems, _displayLabel);
            DataProvider.SetVlaueToDisplay(_heatMapItems,_displayValue);
            OnPropertyChanged("HeatMapItems");
        }

        public ObservableCollection<ICompany> HeatMapItems
        {
            get
            {
                if (_heatMapItems != null)
                {
                    return _heatMapItems;
                }
                throw new NullReferenceException();
            }
            set { throw new NotImplementedException(); }
        }

        public ObservableCollection<ComboItem> Sizes
        {
            get
            {
                return _sizes;
            }
        }

        public ObservableCollection<ComboItem> Colories
        {
            get
            {
                return _colories;
            }
        }

        public ComboItem SelectedSize
        {
            get
            {
                return _selectedSize;
            }
            set
            {
                if (value!=null)
                {
                    _selectedSize = value;
                    UpdateData();
                    OnPropertyChanged("SelectedSize");
                }                
            }
        }

        public ComboItem SelectedColor
        {
            get
            {
                return _selectedColor;
            }
            set
            {
                if (value!=null)
                {
                    _selectedColor = value;
                    UpdateData();
                    OnPropertyChanged("SelectedColor");
                }
                
            }
        }

        /// <summary>
        /// Property on which the data are grouped at HeatMap
        /// </summary>
        public string GroupName
        {
            get
            {
                if (!string.IsNullOrEmpty(_groupName))
                {
                    return _groupName;
                }
                throw new NullReferenceException("GroupName");
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    _groupName = value;
                    StyleName = value=="None" ? "Default" : "Squarified";
                    UpdateData(); //Get grouped data
                    OnPropertyChanged("GroupName");
                }
                else
                    throw new NullReferenceException("GroupName");
            }
        }

        /// <summary>
        /// Property on which the data are filtered at HeatMap
        /// </summary>
        public string FilterKey
        {
            get
            {
                return _filterKey;
            }
            set
            {
                _filterKey = value;
                UpdateData(); //Get filtered data
                OnPropertyChanged("FilterKey");
            }
        }

        public string DisplayLabel
        {
            get
            {
                return _displayLabel;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    _displayLabel = value;                    
                    DataProvider.GetDataByCompanyType(_heatMapItems, value);
                    OnPropertyChanged("DisplayLabel");
                }
            }
        }

        public string DisplayValue
        {
            get
            {
                return _displayValue;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new NullReferenceException("Display value");
                }
                _displayValue = value;
                DataProvider.SetVlaueToDisplay(_heatMapItems,value);
            }
        }

        /// <summary>
        /// CompanyName of heatmap style, displayed in combobox
        /// </summary>
        public string StyleName
        {
            get
            {
                return _styleName;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    _styleName = value;
                    OnPropertyChanged("StyleName");
                }
                else
                    throw new NullReferenceException("StyleName");
            }
        }

        public string AddSymbol
        {
            get
            {
                return _addsymbol;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    _addsymbol = value;
                    SetAddSymbol(value);
                    OnPropertyChanged("AddSymbol");
                }
            }
        }       

        private void SetAddSymbol(string addSymbol)
        {
            switch (addSymbol)
            {
                case "All Market":
                    UpdateData();
                    break;
                case "Sectors"://Ok
                    var sectors = _heatMapItems.OrderBy(x => Guid.NewGuid()).Take(16);//16 diffrent sectors
                    var newsector = sectors.ToList();
                    _heatMapItems = new ObservableCollection<ICompany>(newsector);
                    OnPropertyChanged("HeatMapItems");
                    break;
                case "Customized Portfolio":
                    var window = new CustomPortfolio();
                    window.Show();
                    break;
            }
        }

        public HeatMapVm(Action<string> showMessage)
        {
            if (showMessage==null)
            {
                throw new NullReferenceException("Show message");
            }
            ShowMessage = showMessage;
            //Timer();
        }

        public HeatMapVm(){}

        #region Get Time Interval
        private int _selectedvalue;

        private int SelectedInterval
        {
            get { return _selectedvalue; }
            set { _selectedvalue = value; OnPropertyChanged("SelectedInterval"); }
        }
        #endregion
        #region Timer Refresh

        private void Timer()
        {//refresh Heat Map
            var dispatcherTimer = new DispatcherTimer {Interval = new TimeSpan(0, 0, 10)};
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Start();
        }
        //magic))
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            UpdateData();
            if (ShowMessage!=null)
            {
                ShowMessage("Complete");
            }
            
        }

        #endregion

        private void OnPropertyChanged(string name)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
