﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace HeatMap
{
    /// <summary>
    /// Interaction logic for CustomPortfolio.xaml
    /// </summary>
    public partial class CustomPortfolio : Window
    {
        private readonly HeatMapVm _vm = new HeatMapVm();
        public CustomPortfolio()
        {
            _vm.LoadData();
            DataContext = _vm;
            InitializeComponent();
        }


        private void AddItem(object sender, MouseButtonEventArgs e)
        {
            SelectedItemsListBox.Items.Add(CompanyListBox.SelectedItem);
        }



        private void CheckBox_Checked_1(object sender, RoutedEventArgs e)
        {
            MultiSelectCheckBox.Content = "MultiSelect:On";
            CompanyListBox.SelectionMode = SelectionMode.Multiple;


            MultiSelectCheckBox.Content = "Multiselect:Off";
            CompanyListBox.SelectionMode = SelectionMode.Single;

        }

        private void RadButton_Click(object sender, RoutedEventArgs e)
        {

        

        }



    }
}
