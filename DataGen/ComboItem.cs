﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataGen
{
    public class ComboItem
    {

        private double _value;
        private string _label;

        public string Label
        {
            get
            {
                return _label;
            }
        }

        public double DisplayValue
        {
            get
            {
                return _value;
            }
        }

        public ComboItem()
        {
            _label = "All";
        }

        public override string ToString()
        {
            return _label;
        }

        public ComboItem(double value)
        {
            _value = value;
            _label = value.ToString();
        }
    }
}
