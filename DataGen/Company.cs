﻿using System;
using System.ComponentModel;
using System.Net.NetworkInformation;

namespace DataGen
{
    public class Company : ICompany,INotifyPropertyChanged
    {
        private readonly string _companyName;
        private readonly string _marketName;
        private readonly int _companyCode;
        private readonly string _companyLabel;
        private readonly double _sizeValue;
        private readonly double _colorValue;
        private readonly string _colorBrush = "Red";
        private readonly string _sectorName;
        private string _displayLable;
        private string _displayValue;
        private string _groupValue;

        public string ColorBrush
        {
            get
            {
                return _colorBrush;
            }
        }

        public string CompanyName
        {
            get
            {                                   
                return _companyName;
            }      
        }

        public string MarketName
        {
            get
            {                    
                return _marketName;
            }        
        }

        public string SectorName
        {
            get
            {
                return _sectorName;
            }
        }

        public double SizeValue
        {
            get
            {
                return _sizeValue;
            }
        }

        public double ColorValue
        {
            get
            {
                return _colorValue;
            }
        }

        public int CompanyCode
        {
            get
            {
                return _companyCode;
            }
        }

        public string CompanyLabel
        {
            get
            {
                return _companyLabel;
            }
        }

        public string GroupValue
        {
            get
            {
                if (string.IsNullOrEmpty(_groupValue))
                {
                    throw new NullReferenceException("GroupValue");
                } 
                return _groupValue;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new NullReferenceException("Group value");
                }
                _groupValue = value;
                OnPropertyChanged("GroupValue");
            }
        }

        public string DisplayLabel
        {
            get
            {
                if (string.IsNullOrEmpty(_displayLable))
                {
                    throw new NullReferenceException("Dislay label");
                }
                return _displayLable;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new NullReferenceException("Display label");
                }
                _displayLable = value;
                OnPropertyChanged("DisplayLabel");
            }
        }

        public string DisplayValue
        {
            get
            {
                return _displayValue;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new NullReferenceException("Display value");
                }
                _displayValue = value;
                OnPropertyChanged("DisplayValue");
            }
        }

        private string SetColor(double colorValue)
        {
            if ((colorValue>=-3)&&(colorValue<=-1.4))
            {
                return "#C41E3A";
            }
            if ((colorValue >= -1.5) && (colorValue <= -0.1))
            {
                return "LightCoral";
            }
            if (colorValue == 0.0) 
            {
                return "White";
            }
            if ((colorValue > 0) && (colorValue <= 1.4))
            {
                return "LightGreen";
            }
            if ((colorValue >= 1.5) && (colorValue <= 3.0))
            {
                return "ForestGreen";
            }
            return "";
        }

        /// <summary>
        /// Create new item
        /// </summary>
        /// <param name="companyName">Name of company</param>
        /// <param name="marketName">Name of sector</param>
        /// <param CompanyName="size">Size value</param>
        /// <param CompanyName="color">Color value</param>
        /// <param CompanyName="code">Company code</param>
        /// <param CompanyName="clabel">Ite label</param>
        public Company(string companyName, string marketName, double size, double color, int code, string clabel,string sectorName)
        {
            #region Validate data
            if (string.IsNullOrEmpty(companyName))
            {
                throw new NullReferenceException("CompanyName");
            }
            if (string.IsNullOrEmpty(marketName))
            {
                throw new NullReferenceException("Parent CompanyName");
            }
            if (string.IsNullOrEmpty(clabel))
            {
                throw new NullReferenceException("Company label");
            }
            if (double.IsNaN(size))
            {
                throw new NullReferenceException("Size value");
            }
            if (double.IsNaN(color))
            {
                throw new NullReferenceException("Color value");
            }
            if (string.IsNullOrEmpty(sectorName))
            {
                throw  new NullReferenceException("SectorName");
            }
            #endregion

            _companyName = companyName;
            _marketName = marketName;
            _sizeValue = size;
            _colorValue = color;
            _companyCode = code;
            _companyLabel = clabel;
            _groupValue = marketName;
            _sectorName = sectorName;
            _colorBrush = SetColor(color);
            _displayLable = _companyName;

        }

        private void OnPropertyChanged(string name)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
