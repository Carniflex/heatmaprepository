﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DataGen
{
    /// <summary>
    /// Implement methods to provide items from data source
    /// </summary>
    public class DataProvider
    {
        private readonly IEnumerable<ICompany> _items;

        /// <summary>
        /// Get sample data from Datagen.Generator()
        /// </summary>
        public DataProvider()
        {
            _items = Generator.GenerateData();
        }

        /// <summary>
        /// Get data from your source, use it for integration
        /// </summary>
        /// <param name="dataSource">Data source</param>
        public DataProvider(IEnumerable<ICompany> dataSource)
        {
            if (dataSource != null)
            {
                _items = dataSource;
            }
            else
            {
                throw new NullReferenceException("Data source");
            }
        }

        /// <summary>
        /// Returns all items from dataSource
        /// </summary>
        /// <returns></returns>
        private IEnumerable<ICompany> GetData()
        {
            if (_items != null)
            {
                return _items;
            }
            throw new NullReferenceException("Items are null");
        }

        /// <summary>
        /// Returns items filtered by key
        /// </summary>
        /// <param name="filterKey">Search key</param>
        /// <returns></returns>
        private IEnumerable<ICompany> GetData(string filterKey)
        {
            var allItems = GetData();

            var filterItems = from item in allItems
                              where item.CompanyName.Contains(filterKey) 
                              || item.CompanyLabel.Contains(filterKey)
                              || item.CompanyCode.ToString().Contains(filterKey)
                              || item.SizeValue.ToString().Contains(filterKey)
                              select item;

            return filterItems;
        }

        /// <summary>
        /// Returns items grouped by group key and filtered by filter key
        /// </summary>
        /// <param name="filterKey"></param>
        /// <param name="groupKey"></param>
        /// <returns></returns>
        private IEnumerable<ICompany> GetData(string filterKey, string groupKey)
        {
            if (!string.IsNullOrEmpty(groupKey))
            {
                var items = GetData(filterKey);
                return (GetDataGroupBy(items, groupKey));
            }
            throw new NullReferenceException("Group Key");
        }

        public IEnumerable<ICompany> GetData(string filterKey, string groupKey, ComboItem size, ComboItem color)
        {
            var items = GetData(filterKey, groupKey);

            if (size.Label!="All")
            {
                items = items.Where(item => item.SizeValue == size.DisplayValue);
            }

            if (color.Label!="All")
            {
                items = items.Where(item => item.ColorValue == color.DisplayValue);
            }           

            return items;
        }

        /// <summary>
        /// Returns items grouped by group name
        /// </summary>
        /// <param name="groupName">group name</param>
        /// <returns></returns>
        private IEnumerable<ICompany> GetDataGroupBy(string groupName)
        {
            if (!string.IsNullOrEmpty(groupName))
            {
                var items = GetData();
                return GetDataGroupBy(items, groupName);
            }
            throw new NullReferenceException("GroupName");
        }

        /// <summary>
        /// Return items from IEnumerable grouped by group name
        /// </summary>
        /// <param name="items"></param>
        /// <param name="groupName"></param>
        /// <returns></returns>
        private static IEnumerable<ICompany> GetDataGroupBy(IEnumerable<ICompany> items, string groupName)
        {
            switch (groupName)
            {
                case "None":
                    foreach (var heatMapItem in items)
                    {
                        var tempItem = heatMapItem as Company;
                        if (tempItem != null)
                        {
                            tempItem.GroupValue = heatMapItem.CompanyName;
                            yield return tempItem;
                        }
                    }
                    break;
                case "Sectors":
                    foreach (var heatMapItem in items)
                    {
                        var tempItem = heatMapItem as Company;
                        if (tempItem != null)
                        {
                            tempItem.GroupValue = heatMapItem.MarketName;
                            yield return tempItem;
                        }
                    }
                    break;
                case "Size":
                    foreach (var heatMapItem in items)
                    {
                        var tempItem = heatMapItem as Company;
                        if (tempItem != null)
                        {
                            tempItem.GroupValue = heatMapItem.SizeValue.ToString();
                            yield return tempItem;
                        }
                    }
                    break;
                case "Color":
                    foreach (var heatMapItem in items)
                    {
                        var tempItem = heatMapItem as Company;
                        if (tempItem != null)
                        {
                            tempItem.GroupValue = tempItem.ColorBrush;
                            yield return tempItem;
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Set value to display
        /// </summary>
        /// <param name="items">Items</param>
        /// <param name="companytype">Display value</param>
        public static void GetDataByCompanyType(IEnumerable<ICompany> items, string companytype)
        {
            switch (companytype)
            {
                case "Company Code":
                    foreach (var heatMapItem in items)
                    {
                        var tempItem = heatMapItem as Company;
                        if (tempItem != null) tempItem.DisplayLabel = heatMapItem.CompanyCode.ToString();
                    }
                    break;
                case "Company Label":
                    foreach (var heatMapItem in items)
                    {
                        var tempItem = heatMapItem as Company;
                        if (tempItem != null) tempItem.DisplayLabel = heatMapItem.CompanyLabel;
                    }
                    break;
                case "Company Name":
                    foreach (var heatMapItem in items)
                    {
                        var tempItem = heatMapItem as Company;
                        if (tempItem != null) tempItem.DisplayLabel = heatMapItem.CompanyName;
                    }
                    break;
            }

        }

        public static void SetVlaueToDisplay(IEnumerable<ICompany> items,string displayValue)
        {
            switch (displayValue)
            {
                case "None":
                    foreach (var tempItem in items.OfType<Company>())
                    {
                        tempItem.DisplayValue = " ";
                    }
                    break;
                case "Color":
                    foreach (var tempItem in items.OfType<Company>())
                    {
                        tempItem.DisplayValue = "Color: " + tempItem.ColorValue.ToString();
                    }
                    break;
                case "Size":
                    foreach (var tempItem in items.OfType<Company>())
                    {
                        tempItem.DisplayValue = "Size: " + tempItem.SizeValue.ToString();
                    }
                    break;
                case "Color and Size":
                    foreach (var tempItem in items.OfType<Company>())
                    {
                        tempItem.DisplayValue = "Color: " + tempItem.ColorValue.ToString() + "\n" + "Size: " + tempItem.SizeValue.ToString();
                    }
                    break;
            }
        }
    }
}
