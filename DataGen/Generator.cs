﻿using System.Collections.Generic;

namespace DataGen
{
    public static class Generator
    {
        /// <summary>
        /// Generate sample data
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ICompany> GenerateData()
        {
            yield return new Company(
                companyName: "company12", //Company CompanyName
                marketName: "Russia", //Market CompanyName
                sectorName: "farming",
                size: 14, //Size value
                color: 0.5, //Color value
                code:45453,
                clabel:"abc");
            yield return new Company(
                companyName: "company2d", //Company CompanyName
                marketName: "Russia", //Market CompanyName
                 sectorName: "farming",
                size: 56, //Size value
                color: 0.9, //Color value
                code: 6456,
                clabel: "abc");
            yield return new Company(
                companyName: "company3s", //Company CompanyName
                marketName: "Russia", //Market CompanyName
                size: 25, //Size value
                sectorName:"farming",
                color: 0.9, //Color value
                code: 8678,
                clabel: "abc");
            yield return new Company(
                companyName: "company4g", //Company CompanyName
                marketName: "Russia", //Market CompanyName
                sectorName: "Buildings",
                size: 56, //Size value
                color: 0.9, //Color value
                code: 3456,
                clabel: "abc");
            yield return new Company(
                companyName: "company5f", //Company CompanyName
                marketName: "Russia", //Market CompanyName
                sectorName:"Buildings",
                size: 53, //Size value
                color: -1.9, //Color value
                code: 3455,
                clabel: "abc");
            yield return new Company(
                companyName: "company6s", //Company CompanyName
                marketName: "Russia", //Market CompanyName
                size: 43, //Size value
                sectorName:"Buildings",
                color: -2.9, //Color value
                code: 15435,
                clabel: "abc");
            yield return new Company(
                companyName: "company7g", //Company CompanyName
                marketName: "Russia", //Market CompanyName
                sectorName:"Medicine",
                size: 23, //Size value
                color: 0.9, //Color value
                code: 8678,
                clabel: "abc");
            yield return new Company(
                companyName: "company82", //Company CompanyName
                marketName: "Russia", //Market CompanyName
                size: 34, //Size value
                sectorName:"Medicine",
                color: 0.9, //Color value
                code: 1010,
                clabel: "abc");
            yield return new Company(
                companyName: "company9k", //Company CompanyName
                marketName: "Russia", //Market CompanyName
                sectorName:"Medicine",
                size: 55, //Size value
                color:-0.9, //Color value
                code: 08798,
                clabel: "abc");
            yield return new Company(
                companyName: "company10s", 
                marketName: "Russia", 
                sectorName:"Medicine",
                size: 90, 
                color: 1,
                code:34324,
                clabel:"ccc");
            yield return new Company(
                companyName: "company11f", 
                marketName: "Russia", 
                sectorName:"Medicine",
                size: 50, 
                color: -1.3,
                code:0010,
                clabel:"qwe");
            yield return new Company(
                companyName: "company121", 
                marketName: "Russia", 
                size: 50, 
                sectorName:"farming",
                color: 2,
                code:0100,
                clabel:"low");
            yield return new Company(
                companyName: "company13j", 
                marketName: "Russia",
                sectorName: "University",
                size: 23, 
                color: 2,
                code:1000,
                clabel:"tre");
            yield return new Company(
                companyName: "company1v", 
                marketName: "Uk", 
                sectorName:"farming",
                size: 15, 
                color: -2.3,
                code:0011,
                clabel:"zse");
            yield return new Company(
                companyName: "company2sd", 
                marketName: "Uk", 
                sectorName:"farming",
                size: 20, 
                color: 1.8,
                code:0110,
                clabel:"gta");
            yield return new Company(
                companyName: "company365", 
                marketName: "Uk",
                sectorName:"Medicine",
                size: 19, 
                color: 3,
                code:1110,
                clabel:"fra");
            yield return new Company(
                companyName: "company4ng", 
                marketName: "Uk", 
                sectorName:"Building",
                size: 80, 
                color: 0.7,
                code:1111,
                clabel:"try");
            yield return new Company(
                companyName: "companyer1", 
                marketName: "Usa",
                sectorName:"Medicine",
                size: 42, 
                color: 3,
                code:1101,
                clabel:"tiu");
            yield return new Company(
                companyName: "compafny2", 
                marketName: "Usa", 
                sectorName:"Medicine",
                size: 30, 
                color: 1.5,
                code:0111,
                clabel:"lui");
            yield return new Company(
              companyName: "cohmpany3",
              marketName: "Usa",
              sectorName:"Building",
              size: 30,
              color: 1.5,
              code: 0111,
              clabel: "lui");
            yield return new Company(
              companyName: "compahny4",
              marketName: "Usa",
              sectorName: "University",
              size: 30,
              color: 1.5,
              code: 0111,
              clabel: "lui");
            yield return new Company(
              companyName: "compsdany5",
              marketName: "Usa",
              sectorName:"farming",
              size: 30,
              color: 1.5,
              code: 0111,
              clabel: "lui");
            yield return new Company(
              companyName: "comp54any6",
              marketName: "Usa",
              size: 30,
              sectorName:"University",
              color: 0.0,
              code: 0111,
              clabel: "lui");
            yield return new Company(
              companyName: "co1mpany7",
              marketName: "Usa",
              size: 30,
              sectorName: "University",
              color: 1.0,
              code: 0111,
              clabel: "lui");
            yield return new Company(
              companyName: "compan8y8",
              marketName: "Usa",
              size: 30,
              sectorName: "University",
              color: 2.0,
              code: 0111,
              clabel: "lui");
        }
    }
}
