﻿
namespace DataGen
{
    public interface ICompany
    {
        /// <summary>
        /// Company name
        /// </summary>
        string CompanyName { get; }

        /// <summary>
        /// Company parent name
        /// </summary>
        string MarketName { get;}

        string SectorName { get; }

        /// <summary>
        /// Value of item size
        /// </summary>
        double SizeValue { get; }
        /// <summary>
        /// Value of item color
        /// </summary>
        double ColorValue { get; }

        /// <summary>
        /// Value for item grouping
        /// </summary>
        //string GroupValue { get; set; }

        string CompanyLabel { get;}

        int CompanyCode { get; }

        //string DisplayLabel { get; set; }
    }
}
